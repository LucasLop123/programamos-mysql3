const { Sequelize, Model, DataTypes } = require("sequelize");
const sequelize = new Sequelize('sequelize', 'root', '', {
  host: 'localhost',
  dialect:'mariadb'
});


const User = sequelize.define("user", {
  name: DataTypes.STRING,
  lastname: DataTypes.STRING,
  age: DataTypes.INTEGER
});

const PuntoContacto = sequelize.define("PuntoContacto", {
  Nombre: DataTypes.STRING,
  Valor: DataTypes.STRING
});

User.hasMany(PuntoContacto);
PuntoContacto.belongsTo(User);

sequelize.sync()
async function main(){
  const users = await User.findAll({include:PuntoContacto});

  console.log("All users:", JSON.stringify(users, null, 2));
  
}

main()